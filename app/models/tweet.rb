# == Schema Information
#
# Table name: tweets
#
#  id         :integer          not null, primary key
#  body       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Foreign Keys
#
#  user_id  (user_id => users.id)
#
class Tweet < ApplicationRecord
  belongs_to :user

  # TODO: Move this validation to a Form Object or a different kind of class
  validates :body, length: { maximum: 180 }
  validate :validate_if_same_tweet?


  def validate_if_same_tweet?
   same_tweet = Tweet.where("body = ?", body)
         .where("user_id = ?", user_id)
         .where("created_at >= ?", 24.hours.ago)
         .exists?


    if same_tweet
      # TODO: Move this to the in18n file, please
      errors.add(:body, "You created the same tweet in the last 24 hours")
    end
  end
end
